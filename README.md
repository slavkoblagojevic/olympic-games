## Olympic Games

Application is based on SpringBoot, REST API, ReactJS and Bootstrap.

The application provides work with entities: Competitor, Country and Registration. Application provides search for
contestants by several criteria with pagination, creation of a new contestant, statistics of the number of medals for
each country, registration of competitor, deletion of competitor.


