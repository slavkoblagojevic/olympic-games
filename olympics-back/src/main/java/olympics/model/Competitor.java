package olympics.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Competitor {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	private String fullName;
	
	@Column
	private Long wonMedals;
	
	@Column(nullable = false)
	private String birthDate;
	
	@ManyToOne
	private Country country;
	
	@OneToMany(mappedBy = "competitor", cascade = CascadeType.ALL)
	private List<Registration> registrations = new ArrayList<Registration>();

	public Competitor() {
		super();
	}

	public Competitor(Long id, String fullName, Long wonMedals, String birthDate, Country country,
			List<Registration> registrations) {
		super();
		this.id = id;
		this.fullName = fullName;
		this.wonMedals = wonMedals;
		this.birthDate = birthDate;
		this.country = country;
		this.registrations = registrations;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Long getWonMedals() {
		return wonMedals;
	}

	public void setWonMedals(Long wonMedals) {
		this.wonMedals = wonMedals;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public List<Registration> getRegistrations() {
		return registrations;
	}

	public void setRegistrations(List<Registration> registrations) {
		this.registrations = registrations;
	}

	@Override
	public String toString() {
		return "Competitor [id=" + id + ", fullName=" + fullName + ", wonMedals=" + wonMedals + ", birthDate="
				+ birthDate + ", country=" + country + ", registrations=" + registrations + "]";
	} 
	
	
}
