package olympics.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Registration {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	private String registrationDate;
	
	@Column
	private String discipline;
	
	@ManyToOne
	private Competitor competitor;

	public Registration() {
		super();
	}

	public Registration(Long id, String registrationDate, String discipline, Competitor competitor) {
		super();
		this.id = id;
		this.registrationDate = registrationDate;
		this.discipline = discipline;
		this.competitor = competitor;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getDiscipline() {
		return discipline;
	}

	public void setDiscipline(String discipline) {
		this.discipline = discipline;
	}

	public Competitor getCompetitor() {
		return competitor;
	}

	public void setCompetitor(Competitor competitor) {
		this.competitor = competitor;
	}

	@Override
	public String toString() {
		return "Registration [id=" + id + ", registrationDate=" + registrationDate + ", discipline=" + discipline
				+ ", competitor=" + competitor + "]";
	}
	
	
}
