package olympics.repository;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import olympics.model.Competitor;

@Repository
public interface CompetitorRepository extends JpaRepository<Competitor, Long> {

	List<Competitor> findByCountryId(Long countryId);

	Page<Competitor> findAllByWonMedalsGreaterThanEqualAndWonMedalsLessThanEqual(Long wonMedalsFrom, Long wonMedalsTo, Pageable pageable);

	Page<Competitor> findAllByCountryIdAndWonMedalsGreaterThanEqualAndWonMedalsLessThanEqual(Long countryId,
			Long wonMedalsFrom, Long wonMedalsTo, Pageable pageable);

	//SQL		SELECT * FROM olympics.competitor WHERE id = 3;
	
	@Query("SELECT competitor FROM Competitor competitor WHERE competitor.id LIKE :competitorId")
	Competitor getOneCompetitor(@Param("competitorId") Long id);

	

}
