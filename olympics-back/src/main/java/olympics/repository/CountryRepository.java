package olympics.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import olympics.model.Country;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long>{

	//SQL  SELECT sum(won_medals) FROM olympics.competitor where country_id = 4;
	
	@Query("SELECT SUM(wonMedals) FROM Competitor competitor WHERE competitor.country.id LIKE :id")
	Integer geMedalsForOneCountry(@Param("id") Long id);
}
