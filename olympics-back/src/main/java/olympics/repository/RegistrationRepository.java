package olympics.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import olympics.model.Registration;

@Repository
public interface RegistrationRepository extends JpaRepository<Registration, Long> {

}
