package olympics.service;

import java.util.Optional;

import org.springframework.data.domain.Page;

import olympics.model.Competitor;

public interface CompetitorService {
	Optional<Competitor> getOne(Long id);
	Page<Competitor> getAll(Long countryId, Long wonMedalsFrom, Long wonMedalsTo, int pageNum);
	Competitor save(Competitor competitor);
	Competitor update(Competitor competitor);
	Competitor delete(Long id);
	Competitor findOneCompetitor(Long id);
}
