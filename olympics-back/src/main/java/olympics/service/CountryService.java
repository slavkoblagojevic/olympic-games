package olympics.service;

import java.util.HashMap;
import java.util.List;

import olympics.model.Country;

public interface CountryService{

	List<Country> getAll();
	Country getOne(Long id);
	HashMap<String, Integer> getStatistics();
}
