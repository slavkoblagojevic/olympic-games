package olympics.service;

import olympics.model.Registration;

public interface RegistrationService {
	Registration save(Registration registration);
}
