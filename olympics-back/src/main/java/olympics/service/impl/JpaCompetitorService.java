package olympics.service.impl;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import olympics.model.Competitor;
import olympics.repository.CompetitorRepository;
import olympics.service.CompetitorService;

@Service
public class JpaCompetitorService implements CompetitorService {

	@Autowired
	CompetitorRepository competitorRepository;

	@Override
	public Optional<Competitor> getOne(Long id) {

		return competitorRepository.findById(id);
	}

	@Override
	public Page<Competitor> getAll(Long countryId, Long wonMedalsFrom, Long wonMedalsTo, int pageNum) {

		if (wonMedalsFrom == null) {
			wonMedalsFrom = 0L;
		}
		if (wonMedalsTo == null) {
			wonMedalsTo = Long.MAX_VALUE;
		}

		
		
		if (countryId == null) {
			return competitorRepository.findAllByWonMedalsGreaterThanEqualAndWonMedalsLessThanEqual(wonMedalsFrom,
					wonMedalsTo, PageRequest.of(pageNum, 5));
		}

		return competitorRepository.findAllByCountryIdAndWonMedalsGreaterThanEqualAndWonMedalsLessThanEqual(countryId,
				wonMedalsFrom, wonMedalsTo, PageRequest.of(pageNum, 5));
	}

	@Override
	public Competitor save(Competitor competitor) {

		return competitorRepository.save(competitor);
	}

	@Override
	public Competitor update(Competitor competitor) {

		return competitorRepository.save(competitor);
	}

	@Override
	@Transactional
	public Competitor delete(Long id) {

		Optional<Competitor> optionalCompetitor = competitorRepository.findById(id);

		if (optionalCompetitor.isPresent()) {

			Competitor competitor = optionalCompetitor.get();

			competitorRepository.deleteById(id);

			return competitor;
		}
		return null;
	}

	@Override
	public Competitor findOneCompetitor(Long id) {
			
		return competitorRepository.getOneCompetitor(id);
	}

}
