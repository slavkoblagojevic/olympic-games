package olympics.service.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import olympics.model.Country;
import olympics.repository.CountryRepository;
import olympics.service.CountryService;

@Service
public class JpaCountryService implements CountryService {

	@Autowired
	CountryRepository countryRepository;
	
	@Override
	public List<Country> getAll() {
			
		return countryRepository.findAll();
	}
	
	@Override
	public Country getOne(Long id) {
		return countryRepository.getOne(id);
	}

	@Override
	public HashMap<String, Integer> getStatistics(){
		
		List<Country> countries = countryRepository.findAll();
		
		HashMap<String, Integer> statistics = new HashMap<String, Integer>();
		
		for(Country co : countries) {
			statistics.put(co.getCountryName(), countryRepository.geMedalsForOneCountry(co.getId()));
		}
		
		return statistics;
	}
	
	
}
