package olympics.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import olympics.model.Registration;
import olympics.repository.RegistrationRepository;
import olympics.service.RegistrationService;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;

@Service
public class JpaRegistrationService implements RegistrationService {

	@Autowired
	RegistrationRepository registrationRepository;
	
	@Override
	public Registration save(Registration registration) {
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");  
		LocalDateTime now = LocalDateTime.now();
		String date = dtf.format(now);

		registration.setRegistrationDate(date);
		
		return registrationRepository.save(registration);
	}

}
