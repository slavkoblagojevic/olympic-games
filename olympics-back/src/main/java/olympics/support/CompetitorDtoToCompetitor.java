package olympics.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import olympics.model.Competitor;
import olympics.service.CountryService;
import olympics.web.dto.CompetitorDto;

@Component
public class CompetitorDtoToCompetitor implements Converter<CompetitorDto, Competitor> {

	@Autowired
	CountryService countryService;

	@Override
	public Competitor convert(CompetitorDto competitorDto) {

		Competitor competitor = new Competitor();

		competitor.setId(competitorDto.getId());
		competitor.setFullName(competitorDto.getFullName());
		competitor.setBirthDate(competitorDto.getBirthDate());
		competitor.setWonMedals(competitorDto.getWonMedals());
		competitor.setCountry(countryService.getOne(competitorDto.getCountryId()));

		return competitor;
	}

	public List<Competitor> convert(List<CompetitorDto> competitorsDto) {

		List<Competitor> competitors = new ArrayList<Competitor>();

		for(CompetitorDto com : competitorsDto) {
			competitors.add(convert(com));
		}
		
		return competitors;
	}

}
