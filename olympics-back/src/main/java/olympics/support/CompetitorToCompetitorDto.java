package olympics.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import olympics.model.Competitor;
import olympics.web.dto.CompetitorDto;

@Component
public class CompetitorToCompetitorDto implements Converter<Competitor, CompetitorDto> {

	@Override
	public CompetitorDto convert(Competitor competitor) {

		CompetitorDto competitorDto = new CompetitorDto();

		competitorDto.setId(competitor.getId());
		competitorDto.setFullName(competitor.getFullName());
		competitorDto.setBirthDate(competitor.getBirthDate());
		competitorDto.setWonMedals(competitor.getWonMedals());
		competitorDto.setCountryId(competitor.getCountry().getId());
		competitorDto.setCountryCode(competitor.getCountry().getCountryCode());

		return competitorDto;
	}

	public List<CompetitorDto> convert(List<Competitor> competitors) {

		List<CompetitorDto> competitorsDto = new ArrayList<CompetitorDto>();

		for (Competitor c : competitors) {
			competitorsDto.add(convert(c));
		}

		return competitorsDto;
	}
}
