package olympics.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import olympics.model.Country;
import olympics.web.dto.CountryDto;

@Component
public class CountryToCountryDto implements Converter<Country, CountryDto>{

	@Override
	public CountryDto convert(Country country) {
		
		CountryDto countryDto = new CountryDto();
		
		countryDto.setId(country.getId());
		countryDto.setCountryName(country.getCountryName());
		countryDto.setCountryCode(country.getCountryCode());
		
		return countryDto;
	}

	public List<CountryDto> convert(List<Country> countries){
		
		List<CountryDto> countriesDto = new ArrayList<CountryDto>();
		
		for(int i=0; i<countries.size(); i++) {
			countriesDto.add(convert(countries.get(i)));
		}
		
		return countriesDto;
	}
}
