package olympics.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import olympics.model.Registration;
import olympics.service.CompetitorService;
import olympics.web.dto.RegistrationDto;

@Component
public class RegistrationDtoToRegistration implements Converter<RegistrationDto, Registration>{

	@Autowired
	CompetitorService competitorService;
	
	@Override
	public Registration convert(RegistrationDto registrationDto) {
	
		Registration registration = new Registration();
		
		registration.setDiscipline(registrationDto.getDiscipline());
		registration.setCompetitor(competitorService.findOneCompetitor(registrationDto.getCompetitorId()));
		
		return registration;
	}

}
