package olympics.support;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import olympics.model.Registration;
import olympics.web.dto.RegistrationDto;

@Component
public class RegistrationToRegistarationDto implements Converter<Registration, RegistrationDto> {

	@Override
	public RegistrationDto convert(Registration registration) {
		
		RegistrationDto registrationDto = new RegistrationDto();
		
		registrationDto.setCompetitorId(registration.getCompetitor().getId());
		registrationDto.setDiscipline(registration.getDiscipline());
		
		return registrationDto;
	}

}
