package olympics.web.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import olympics.model.Competitor;
import olympics.service.CompetitorService;
import olympics.support.CompetitorDtoToCompetitor;
import olympics.support.CompetitorToCompetitorDto;
import olympics.web.dto.CompetitorDto;

@RestController
@RequestMapping(value = "/api/competitors", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class ApiCompetitorController {

	@Autowired
	CompetitorToCompetitorDto toCompetitorDto;
	
	@Autowired
	CompetitorDtoToCompetitor toCompetitor;

	@Autowired
	CompetitorService competitorService;

	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping
	public ResponseEntity<List<CompetitorDto>> getAll(
			@RequestParam(required = false) Long countryId,
			@RequestParam(required = false) Long wonMedalsFrom, 
			@RequestParam(required = false) Long wonMedalsTo,
			@RequestParam(defaultValue = "0") int pageNum) {

//		List<Competitor> competitors = competitorService.getAll(countryId, wonMedalsFrom, wonMedalsTo);  => without Pagination

		Page<Competitor> page = competitorService.getAll(countryId, wonMedalsFrom, wonMedalsTo, pageNum);
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Total-Pages", Integer.toString(page.getTotalPages()));
		
		return new ResponseEntity<>(toCompetitorDto.convert(page.getContent()),headers, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping(value = "/{id}")
	public ResponseEntity<CompetitorDto> getOne(@PathVariable Long id) {

		Optional<Competitor> competitor = competitorService.getOne(id);

		if (competitor.isPresent()) {
			return new ResponseEntity<>(toCompetitorDto.convert(competitor.get()), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CompetitorDto> create(@Valid @RequestBody CompetitorDto competitorDto){
		
		Competitor competitor = toCompetitor.convert(competitorDto);
		
		Competitor savedCompetitor = competitorService.save(competitor);
		
		return new ResponseEntity<>(toCompetitorDto.convert(savedCompetitor), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping(value = "/{id}")
	public ResponseEntity<CompetitorDto> update( @PathVariable Long id, @Valid @RequestBody CompetitorDto competitorDto){
		
		Competitor competitor = toCompetitor.convert(competitorDto);
		competitor.setId(id);
		Competitor updatedCompetitor = competitorService.update(competitor);
		
		return new ResponseEntity<>(toCompetitorDto.convert(updatedCompetitor), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<CompetitorDto> delete(@PathVariable Long id){
		
		Competitor competitor = competitorService.delete(id);
		
		if(competitor == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(toCompetitorDto.convert(competitor), HttpStatus.OK);
	}
}
