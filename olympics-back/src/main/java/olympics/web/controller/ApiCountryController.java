package olympics.web.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import olympics.model.Country;
import olympics.service.CountryService;
import olympics.support.CountryToCountryDto;
import olympics.web.dto.CountryDto;

@RestController
@RequestMapping("api/countries")
public class ApiCountryController {
	
	@Autowired
	CountryService countryService;
	
	@Autowired
	CountryToCountryDto toCountryDto;
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping
	public ResponseEntity<List<CountryDto>> getAll(){
		
		List<Country> countries = countryService.getAll();
		
		return new ResponseEntity<>(toCountryDto.convert(countries), HttpStatus.OK);
	}
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping("/statistics")
	public HashMap<String, Integer> statistics(){
		
		HashMap<String, Integer> statistisc = countryService.getStatistics();
		
		return statistisc;
	}
}
