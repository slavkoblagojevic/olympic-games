package olympics.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import olympics.model.Registration;
import olympics.service.RegistrationService;
import olympics.support.RegistrationDtoToRegistration;
import olympics.support.RegistrationToRegistarationDto;
import olympics.web.dto.RegistrationDto;

@RestController
@RequestMapping(value = "/api/registrations")
public class ApiRegistrationController {

	@Autowired
	RegistrationDtoToRegistration toRegistration;

	@Autowired
	RegistrationToRegistarationDto toRegistarationDto;

	@Autowired
	RegistrationService registrationService;
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@PostMapping
	public ResponseEntity<RegistrationDto> create(@RequestBody RegistrationDto registrationDto) {
		
		Registration registration = toRegistration.convert(registrationDto);
		
		Registration savedRegistration = registrationService.save(registration);
		
		return new ResponseEntity<>(toRegistarationDto.convert(savedRegistration), HttpStatus.OK);
	}
}
