package olympics.web.dto;

public class CompetitorDto {
	
	private Long id;
	
	private String fullName;
	
	private Long wonMedals;
	
	private String birthDate;
	
	private Long countryId;
	
	private String countryCode;

	public CompetitorDto() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Long getWonMedals() {
		return wonMedals;
	}

	public void setWonMedals(Long wonMedals) {
		this.wonMedals = wonMedals;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	
	
}
