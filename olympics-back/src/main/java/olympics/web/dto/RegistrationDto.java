package olympics.web.dto;

public class RegistrationDto {

	private String discipline;

	private Long competitorId;

	public RegistrationDto() {
		super();
	}

	public String getDiscipline() {
		return discipline;
	}

	public void setDiscipline(String discipline) {
		this.discipline = discipline;
	}

	public Long getCompetitorId() {
		return competitorId;
	}

	public void setCompetitorId(Long competitorId) {
		this.competitorId = competitorId;
	}
	
	
}
