INSERT INTO `user` (id, username, password, role)
              VALUES (1,'miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','ADMIN');
INSERT INTO `user` (id, username, password, role)
              VALUES (2,'tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','KORISNIK');

INSERT INTO `user` (id, username, password, role)
              VALUES (3,'petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','KORISNIK');
            
INSERT INTO `user` (id, username, password, role)
              VALUES (4,'srdjan','$2a$12$fxagy0tjs2bOmCy/.3nwL.qSW8TvqWbaVG6fiPCRm.ljaVovqobvm','KORISNIK');

INSERT INTO `user` (id, username, password, role)
              VALUES (5,'srdjan1','$2a$12$ojxhd54XXknRkLV3esAMV.B/ADghP3nYWJF.4YXVAwISsapGTbgcy','ADMIN');

              
INSERT INTO country (id, country_name, country_code) VALUES (1, 'Serbia', 'SRB');              
INSERT INTO country (id, country_name, country_code) VALUES (2, 'France', 'FRA');
INSERT INTO country (id, country_name, country_code) VALUES (3, 'Russia', 'RUS');
INSERT INTO country (id, country_name, country_code) VALUES (4, 'Germany', 'GER');

INSERT INTO competitor (id, full_name, won_medals, birth_date, country_id) VALUES (1, 'Novak Djokovic', 2, '22. May 1987.', 1);
INSERT INTO competitor (id, full_name, won_medals, birth_date, country_id) VALUES (2, 'Nemanja Bjelica', 2, '9. May 1988.', 1);
INSERT INTO competitor (id, full_name, won_medals, birth_date, country_id) VALUES (3, 'Tomas Miller', 4, '13. September 1989.', 4);
INSERT INTO competitor (id, full_name, won_medals, birth_date, country_id) VALUES (4, 'Andrej Kirilenko', 2, '18. February 1981.', 3);
INSERT INTO competitor (id, full_name, won_medals, birth_date, country_id) VALUES (5, 'Jasna Sekaric', 9, '17. December 1965', 1);
INSERT INTO competitor (id, full_name, won_medals, birth_date, country_id) VALUES (6, 'Ivana Maksimovic', 3, '2. May 1990.', 1);
INSERT INTO competitor (id, full_name, won_medals, birth_date, country_id) VALUES (7, 'Tony Parker', 1, '17. May 1982.', 2);
INSERT INTO competitor (id, full_name, won_medals, birth_date, country_id) VALUES (8, 'Igor Rakocevic', 1, '29. March 1978.', 1);
INSERT INTO competitor (id, full_name, won_medals, birth_date, country_id) VALUES (9, 'Andrija Prlainovic', 6, '28. april 1987.', 1);
INSERT INTO competitor (id, full_name, won_medals, birth_date, country_id) VALUES (10, 'Thierry Henry', 1, '17. August 1977.', 2);
INSERT INTO competitor (id, full_name, won_medals, birth_date, country_id) VALUES (11, 'Amélie Mauresmo', 1, '5. July 1979.', 2);
INSERT INTO competitor (id, full_name, won_medals, birth_date, country_id) VALUES (12, 'Nikolaj Valujev', 1, '21. August 1973.', 3);
INSERT INTO competitor (id, full_name, won_medals, birth_date, country_id) VALUES (13, 'Oliver Kahn', 1, '15. June 1969.', 4);

INSERT INTO registration(id, registration_date, discipline, competitor_id) VALUES (1, '10. novembar 2022', 'Tenis', 1);
INSERT INTO registration(id, registration_date, discipline, competitor_id) VALUES (2, '10. novembar 2022', 'Basketball', 4);
INSERT INTO registration(id, registration_date, discipline, competitor_id) VALUES (3, '11. novembar 2022', 'Football', 3);
INSERT INTO registration(id, registration_date, discipline, competitor_id) VALUES (4, '12. novembar 2022', 'Basketball', 2);