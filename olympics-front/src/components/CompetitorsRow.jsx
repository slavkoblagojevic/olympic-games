import React from "react";
import { Button } from "react-bootstrap";
import { withParams, withNavigation } from "../routeconf";

const CompetitorsRow = (props) => {
  const { competitor } = props;

  return (
    <tr key={competitor.id}>
      <td>{competitor.fullName}</td>
      <td>{competitor.countryCode}</td>
      <td>{competitor.birthDate}</td>
      <td>{competitor.wonMedals}</td>
      <td>
        <Button variant="danger" onClick={() => props.doDelete(competitor.id)}>
          Delete
        </Button>{" "}
        <Button
          variant="info"
          onClick={() =>
            props.navigate(
              "/registration/" + competitor.id + "/" + competitor.fullName
            )
          }
        >
          Register
        </Button>
      </td>
    </tr>
  );
};

export default withNavigation(withParams(CompetitorsRow));
