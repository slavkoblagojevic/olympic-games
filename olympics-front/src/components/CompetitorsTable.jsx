import React from "react";
import { Table } from "react-bootstrap";
import CompetitorsRow from "./CompetitorsRow";

const CompetitorsTable = (props) => {
  const competitorsRow = props.competitorsList.map((competitor, index) => {
    return (
      <CompetitorsRow
        key={index}
        competitor={competitor}
        doDelete={props.doDelete}
      />
    );
  });

  return (
    <div>
      <Table striped bordered hover>
        <thead className="thead-dark">
          <tr>
            <th>Name</th>
            <th>Country</th>
            <th>Birth Date</th>
            <th>Medals</th>
            <th></th>
          </tr>
        </thead>
        <tbody>{competitorsRow}</tbody>
      </Table>
    </div>
  );
};

export default CompetitorsTable;
