import React, { useState } from "react";
import { Button } from "react-bootstrap";

const Pagination = (props) => {
  const { totalPages, currentPage, setCurrentPage } = props;

  const pageBack = () => {
    setCurrentPage(currentPage - 1);
  };

  const pageNext = () => {
    setCurrentPage(currentPage + 1);
  };

  return (
    <div style={{ float: "right" }}>
      <Button
        style={{ margin: 10 }}
        variant="info"
        disabled={currentPage === 0}
        onClick={() => pageBack()}
      >
        Previous
      </Button>
      <Button
        variant="info"
        disabled={currentPage === totalPages - 1}
        onClick={() => pageNext()}
      >
        Next
      </Button>
    </div>
  );
};

export default Pagination;
