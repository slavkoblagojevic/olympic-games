import React, { useState, useEffect } from "react";
import { Button, Form } from "react-bootstrap";
import TestAxios from "../apis/TestAxios";

const Search = (props) => {
  const {
    countryId,
    setCountryId,
    wonMedalsFrom,
    setWonMedalsFrom,
    wonMedalsTo,
    setWonMedalsTo,
  } = props;

  const [countries, setCountries] = useState([]);

  const getCountries = () => {
    TestAxios.get("/countries")
      .then((res) => {
        setCountries(res.data);
        console.log("countries", res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    getCountries();
  }, []);

  const countryOptions = countries.map((country) => {
    return (
      <option key={country.id} value={country.id}>
        {country.countryName}
      </option>
    );
  });

  return (
    <Form>
      <Form.Group>
        <Form.Label>Select Country</Form.Label>
        <Form.Control
          as="select"
          value={countryId}
          onChange={(e) => {
            console.log(e.target.value);
            setCountryId(e.target.value);
          }}
        >
          <option value={-1}>--Country--</option>
          {countryOptions}
        </Form.Control>
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Label>Medals won from</Form.Label>
        <Form.Control
          type="number"
          placeholder="1"
          value={wonMedalsFrom}
          onChange={(event) => {
            setWonMedalsFrom(event.target.value);
            console.log(event.target.value);
          }}
        />
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Label>Medals won to</Form.Label>
        <Form.Control
          type="number"
          placeholder="10"
          value={wonMedalsTo}
          onChange={(event) => setWonMedalsTo(event.target.value)}
        />
      </Form.Group>
      <Button onClick={() => props.onClickSearch()}>Search</Button>
    </Form>
  );
};

export default Search;
