import React from "react";
import ReactDOM from "react-dom";
import { Route, HashRouter as Router, Routes } from "react-router-dom";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Competitors from "./pages/Competitors";
import AddCompetitor from "./pages/AddCompetitor";
import Statistics from "./pages/Statistics";
import Registration from "./pages/Registration";

class App extends React.Component {
  render() {
    return (
      <div>
        <Router>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/login" element={<Login />} />
            <Route path="/competitors" element={<Competitors />} />
            <Route path="/add-competitor" element={<AddCompetitor />} />
            <Route path="/statistics" element={<Statistics />} />
            <Route path="/registration/:id/:name" element={<Registration />} />
          </Routes>
        </Router>
      </div>
    );
  }
}

ReactDOM.render(<App />, document.querySelector("#root"));
