import React from "react";
import { Navbar, Nav, Button, Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import { logout } from "../services/auth";

const NavBarLayout = (props) => {
  return (
    <>
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand as={Link} to="/">
          Olimpics
        </Navbar.Brand>
        <Nav className="me-auto">
          <Nav.Link as={Link} to="/">
            Home
          </Nav.Link>
          <Nav.Link as={Link} to="/competitors">
            Competitors
          </Nav.Link>
          <Nav.Link as={Link} to="/add-competitor">
            Add Competitor
          </Nav.Link>
        </Nav>
        {window.localStorage["jwt"] ? (
          <Button onClick={() => logout()}>Logout</Button>
        ) : (
          <Nav.Link as={Link} to="/login">
            Login
          </Nav.Link>
        )}
      </Navbar>
      <Container>{props.children}</Container>
    </>
  );
};

export default NavBarLayout;
