import React, { useState, useEffect } from "react";
import NavBarLayout from "../layouts/NavBarLayout";
import { Form, Button } from "react-bootstrap";
import { withNavigation, withParams } from "../routeconf";
import TestAxios from "../apis/TestAxios";

const AddCompetitor = (props) => {
  const [fullName, setFullName] = useState("");
  const [wonMedals, setWonMedals] = useState("");
  const [birthDate, setBirthDate] = useState("");
  const [countryId, setCountryId] = useState("");

  const [countries, setCountries] = useState([]);

  const getCountries = () => {
    TestAxios.get("/countries")
      .then((res) => {
        setCountries(res.data);
        console.log("countries from add", res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    getCountries();
  }, []);

  const countryOptions = countries.map((country) => {
    return (
      <option key={country.id} value={country.id}>
        {country.countryName}
      </option>
    );
  });

  const create = () => {
    let params = {
      fullName: fullName,
      wonMedals: wonMedals,
      birthDate: birthDate,
      countryId: countryId,
    };

    TestAxios.post("/competitors", params)
      .then((res) => {
        console.log(res);
        alert("Competitor was added successfully!");
        goToCompetitors();
      })
      .catch((error) => {
        console.log(error);
        alert("Error occured please try again!");
      });
  };

  const goToCompetitors = () => {
    props.navigate("/competitors");
  };

  return (
    <NavBarLayout>
      <Form>
        <h2>Create New Competitor</h2>
        <Form.Group>
          <Form.Label>Full name</Form.Label>
          <Form.Control
            placeholder="John Smith"
            required
            value={fullName}
            onChange={(e) => setFullName(e.target.value)}
          ></Form.Control>
        </Form.Group>
        <Form.Group>
          <Form.Label>Birthday</Form.Label>
          <Form.Control
            placeholder="01. January 2000."
            value={birthDate}
            onChange={(e) => setBirthDate(e.target.value)}
          ></Form.Control>
        </Form.Group>
        <Form.Group>
          <Form.Label>Medals won</Form.Label>
          <Form.Control
            value={wonMedals}
            placeholder="1"
            type="number"
            onChange={(e) => setWonMedals(e.target.value)}
          ></Form.Control>
        </Form.Group>
        <Form.Group>
          <Form.Label>Country</Form.Label>
          <Form.Control
            as="select"
            onChange={(e) => setCountryId(e.target.value)}
          >
            <option value="">--Country--</option>
            {countryOptions}
          </Form.Control>
        </Form.Group>
        <Button style={{ marginTop: 10 }} onClick={() => create()}>
          Create
        </Button>
      </Form>
    </NavBarLayout>
  );
};

export default withNavigation(withParams(AddCompetitor));
