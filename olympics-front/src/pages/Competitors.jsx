import React, { useEffect, useState } from "react";
import NavBarLayout from "../layouts/NavBarLayout";
import TestAxios from "../apis/TestAxios";
import CompetitorsTable from "../components/CompetitorsTable";
import { Button, Card } from "react-bootstrap";
import { withParams, withNavigation } from "../routeconf";
import Search from "../components/Search";
import Pagination from "../components/Pagination";

const Competitors = (props) => {
  const [competitors, setCompetitors] = useState([]);

  const [countryId, setCountryId] = useState("");
  const [wonMedalsFrom, setWonMedalsFrom] = useState("");
  const [wonMedalsTo, setWonMedalsTo] = useState("");

  const [totalPages, setTotalPages] = useState(1);
  const [currentPage, setCurrentPage] = useState(0);

  const getCompetitors = () => {
    const config = {
      params: {
        wonMedalsFrom: wonMedalsFrom,
        wonMedalsTo: wonMedalsTo,
        pageNum: currentPage,
      },
    };

    if (countryId > 0) {
      config.params.countryId = countryId;
    }

    TestAxios.get("/competitors", config)
      .then((res) => {
        console.log("competitors: ", res);
        setCompetitors(res.data);
        setTotalPages(res.headers["total-pages"]);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    getCompetitors();
  }, [currentPage, competitors]);

  const doDelete = (competitorId) => {
    TestAxios.delete("/competitors/" + competitorId)
      .then((res) => {
        alert("Competitor was deleted successfully!");
        getCompetitors(0);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <NavBarLayout>
      <h2>
        <b>Competitors</b>
      </h2>
      <Card style={{ padding: "5px" }}>
        <Card.Body>
          <Search
            countryId={countryId}
            setCountryId={setCountryId}
            wonMedalsFrom={wonMedalsFrom}
            setWonMedalsFrom={setWonMedalsFrom}
            wonMedalsTo={wonMedalsTo}
            setWonMedalsTo={setWonMedalsTo}
            onClickSearch={getCompetitors}
          />
        </Card.Body>
      </Card>
      <Button
        variant="success"
        style={{ margin: 10 }}
        onClick={() => props.navigate("/add-competitor")}
      >
        New Competitor
      </Button>
      <Button
        variant="warning"
        style={{ margin: 10 }}
        onClick={() => props.navigate("/statistics")}
      >
        Statistics per Country
      </Button>
      <Pagination
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        totalPages={totalPages}
      />
      <CompetitorsTable competitorsList={competitors} doDelete={doDelete} />
    </NavBarLayout>
  );
};

export default withNavigation(withParams(Competitors));
