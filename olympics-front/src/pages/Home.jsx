import React from "react";
import { Carousel } from "react-bootstrap";
import NavBarLayout from "../layouts/NavBarLayout";

const Home = () => {
  return (
    <NavBarLayout>
      <h2>Welcome to Olympic Games</h2>
      <Carousel>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src="maksimovic.jpeg "
            alt="Ivana Maksimovic"
          />
          <Carousel.Caption>
            <h3>Ivana Maksimovic</h3>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img className="d-block w-100" src="novak.jpg" alt="Novak Djokovic" />

          <Carousel.Caption>
            <h3>Novak Djokovic</h3>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src="nemanja.jpg"
            alt="Nemanja Bjelica"
          />

          <Carousel.Caption>
            <h3>Nemanja Bjelica</h3>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    </NavBarLayout>
  );
};

export default Home;
