import React, { useState } from "react";
import TestAxios from "../apis/TestAxios";
import { Button, Form, Container } from "react-bootstrap";
import { withNavigation, withParams } from "../routeconf";
import NavBarLayout from "../layouts/NavBarLayout";

const Registration = (props) => {
  const [discipline, setDiscipline] = useState("");

  const createRegistration = () => {
    let params = {
      competitorId: props.params.id,
      discipline: discipline,
    };

    TestAxios.post("/registrations", params)
      .then((res) => {
        console.log(res);
        alert("Competitor registered successfully!");
        props.navigate("/competitors");
      })
      .catch((error) => {
        console.log(error);
        alert("Error occured please try again!");
      });
  };
  return (
    <NavBarLayout>
      <Container>
        <Form>
          <h4>Registration</h4>
          <Form.Group>
            <Form.Label>Competitor:</Form.Label>
            <Form.Control
              type="text"
              disabled
              placeholder={props.params.name}
            ></Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label>Enter Discipline:</Form.Label>
            <Form.Control
              type="text"
              name="discipline"
              value={discipline}
              onChange={(e) => setDiscipline(e.target.value)}
            ></Form.Control>
          </Form.Group>
          <Button style={{ margin: 5 }} onClick={() => createRegistration()}>
            Register
          </Button>
        </Form>
      </Container>
    </NavBarLayout>
  );
};

export default withNavigation(withParams(Registration));
