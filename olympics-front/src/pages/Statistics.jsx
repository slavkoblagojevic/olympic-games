import React, { useState, useEffect } from "react";
import TestAxios from "../apis/TestAxios";
import { Card, Table, Container } from "react-bootstrap";
import NavBarLayout from "../layouts/NavBarLayout";

const Statistics = () => {
  const [statistics, setStatistics] = useState({});

  const getStatistics = () => {
    TestAxios.get("/countries/statistics")
      .then((res) => {
        console.log("stat", res.data);
        setStatistics(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    getStatistics();
  }, []);

  const keyList = Object.keys(statistics);
  const renderStatistics = keyList.map((key, index) => {
    return (
      <tr key={index}>
        <td>{key}</td>
        <td>{statistics[key] ? statistics[key] : "No medals"}</td>
      </tr>
    );
  });

  return (
    <NavBarLayout>
      <Container className="col-8">
        <Card style={{ padding: "5px" }}>
          <Card.Body>
            <Card.Title style={{ fontWeight: "bold" }}>STATISTICS</Card.Title>
            <Card.Text>Total medals per Country</Card.Text>
            <Table striped bordered hover>
              <thead className="thead-dark">
                <tr>
                  <th>Country</th>
                  <th>Medals</th>
                </tr>
              </thead>
              <tbody>{renderStatistics}</tbody>
            </Table>
          </Card.Body>
          <Card.Footer>
            <small className="text-muted">Last updated 1 min ago</small>
          </Card.Footer>
        </Card>
      </Container>
    </NavBarLayout>
  );
};

export default Statistics;
